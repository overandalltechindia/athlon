﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_ContactDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "contact",
                table: "Vendor",
                maxLength: 12,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "contact",
                table: "Customer",
                maxLength: 12,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "contact",
                table: "Vendor");

            migrationBuilder.DropColumn(
                name: "contact",
                table: "Customer");
        }
    }
}
