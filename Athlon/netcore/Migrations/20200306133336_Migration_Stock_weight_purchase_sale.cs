﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_Stock_weight_purchase_sale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "weight",
                table: "ReceivingLine",
                newName: "weightReceive");

            migrationBuilder.AddColumn<float>(
                name: "totalWeight",
                table: "ShipmentLine",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "weightShipment",
                table: "ShipmentLine",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "totalWeight",
                table: "ReceivingLine",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "totalWeight",
                table: "ShipmentLine");

            migrationBuilder.DropColumn(
                name: "weightShipment",
                table: "ShipmentLine");

            migrationBuilder.DropColumn(
                name: "totalWeight",
                table: "ReceivingLine");

            migrationBuilder.RenameColumn(
                name: "weightReceive",
                table: "ReceivingLine",
                newName: "weight");
        }
    }
}
