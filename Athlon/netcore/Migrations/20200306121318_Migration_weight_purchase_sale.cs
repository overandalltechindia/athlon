﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_weight_purchase_sale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "palletBundle",
                table: "SalesOrderLine",
                newName: "totalWeight");

            migrationBuilder.RenameColumn(
                name: "palletBundle",
                table: "PurchaseOrderLine",
                newName: "totalWeight");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "totalWeight",
                table: "SalesOrderLine",
                newName: "palletBundle");

            migrationBuilder.RenameColumn(
                name: "totalWeight",
                table: "PurchaseOrderLine",
                newName: "palletBundle");
        }
    }
}
