﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_USDPrice_EmployeeName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "employeeName",
                table: "SalesOrder",
                maxLength: 30,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "usdRate",
                table: "SalesOrder",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "employeeName",
                table: "PurchaseOrder",
                maxLength: 30,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "usdRate",
                table: "PurchaseOrder",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "priceInUSD",
                table: "Product",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "employeeName",
                table: "SalesOrder");

            migrationBuilder.DropColumn(
                name: "usdRate",
                table: "SalesOrder");

            migrationBuilder.DropColumn(
                name: "employeeName",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "usdRate",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "priceInUSD",
                table: "Product");
        }
    }
}
