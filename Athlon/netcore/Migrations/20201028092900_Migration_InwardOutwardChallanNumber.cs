﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_InwardOutwardChallanNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "deliveryChallanNumber",
                table: "SalesOrder",
                maxLength: 38,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "deliveryInwardNumber",
                table: "PurchaseOrder",
                maxLength: 38,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "deliveryChallanNumber",
                table: "SalesOrder");

            migrationBuilder.DropColumn(
                name: "deliveryInwardNumber",
                table: "PurchaseOrder");
        }
    }
}
