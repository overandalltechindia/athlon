﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Customer : INetcoreMasterChild, IBaseAddress
    {
        public Customer()
        {
            this.createdAt = DateTime.Now.Date;
            this.registrationDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.registrationExpDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.serviceDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;

        }

        [StringLength(38)]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [StringLength(50)]
        [Display(Name = "Customer Name")]
        [Required]
        public string customerName { get; set; }


        [Display(Name = "Registration Date")]
        public DateTime registrationDate { get; set; }


        [Display(Name = "Expiry Date")]
        public DateTime registrationExpDate { get; set; }


        [Display(Name = "Service Date")]
        public DateTime serviceDate { get; set; }


        [StringLength(50)]
        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "GYM Type")]
        public BusinessSize size { get; set; }

        //IBaseAddress
        [Display(Name = "Address")]
        [Required]
        [StringLength(256)]
        public string street1 { get; set; }

        [Display(Name = "PIN Code")]
        [StringLength(10)]
        public string street2 { get; set; } // use as Pin code

        [Display(Name = "City")]
        [StringLength(30)]
        public string city { get; set; }

        [Display(Name = "State")]
        [StringLength(30)]
        public string province { get; set; }

        [Display(Name = "Country")]
        [StringLength(30)]
        public string country { get; set; }


        [Display(Name = "Primary Contact")]
        [StringLength(12)]
        public string contact { get; set; }


        //IBaseAddress

        [Display(Name = "Customer Contacts")]
        public List<CustomerLine> customerLine { get; set; } = new List<CustomerLine>();
    }
}
