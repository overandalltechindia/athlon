﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class SalesOrder : INetcoreMasterChild
    {
        public SalesOrder()
        {
            this.createdAt = DateTime.Now.Date;
            this.salesOrderNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#SO";
            this.soDate = DateTime.Now.Date;
            this.deliveryDate = this.soDate.AddDays(5);
            this.salesOrderStatus = SalesOrderStatus.Draft;
            this.totalDiscountAmount = 0m;
            this.totalOrderAmount = 0m;
        }

        [StringLength(38)]
        [Display(Name = "Outward Order Id")]
        public string salesOrderId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Outward Number")]
        public string salesOrderNumber { get; set; }

        [StringLength(38)]
        [Display(Name = "Delivery Challan Number")]
        public string deliveryChallanNumber { get; set; }

        [Display(Name = "Mode of Payment")]
        public TOP top { get; set; }

        [Display(Name = "Outward Date")]
        [Required]
        public DateTime soDate { get; set; }

        [Display(Name = "Delivery Date")]
        public DateTime deliveryDate { get; set; }

        [StringLength(256)]
        [Display(Name = "Delivery Address")]
        public string deliveryAddress { get; set; }

        [StringLength(30)]
        [Display(Name = "Vehicle Number")]
        public string vehicleNumber { get; set; }

        [StringLength(30)]
        [Display(Name = "Employee Name")]
        public string employeeName { get; set; }

        [Display(Name = "USD Rate")]
        public decimal? usdRate { get; set; }

        //[StringLength(30)]
        //[Display(Name = "Reference Number (External)")]
        //public string referenceNumberExternal { get; set; }

        [StringLength(100)]
        [Display(Name = "Service Description")]
        public string description { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        //[StringLength(30)]
        //[Required]
        //[Display(Name = "PIC Internal")]
        //public string picInternal { get; set; }

        //[StringLength(30)]
        //[Required]
        //[Display(Name = "PIC Customer")]
        //public string picCustomer { get; set; }

        [Display(Name = "Outward Status")]
        public SalesOrderStatus salesOrderStatus { get; set; }

        [Display(Name = "Total Discount")]
        public decimal totalDiscountAmount { get; set; }

        [Display(Name = "Total Order")]
        public decimal totalOrderAmount { get; set; }

        [Display(Name = "Sales Shipment Number")]
        public string salesShipmentNumber { get; set; }

        [Display(Name = "Sales Order Lines")]
        public List<SalesOrderLine> salesOrderLine { get; set; } = new List<SalesOrderLine>();
    }
}
