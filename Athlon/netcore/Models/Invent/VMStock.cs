﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class VMStock
    {
        [Display(Name = "Model No")]
        public string Product { get; set; }

        [Display(Name = "product Name")]
        public string ProductName { get; set; }


        [Display(Name = "Minimum Stock")]
        public float MinimumStock { get; set; }


        [Display(Name = "product Category")]
        public string ProductCategoryName { get; set; }

        //[Display(Name = "Warehouse")]
        //public string Warehouse { get; set; }

        [Display(Name = "Qty PO")]
        public float QtyPO { get; set; }
        [Display(Name = "Weight PO")]
        public float WeightPO { get; set; }


        [Display(Name = "Qty Receiving")]
        public float QtyReceiving { get; set; }


        [Display(Name = "Pending Receive")]
        public float PendingReceive { get; set; }

        [Display(Name = "Pending Shipment")]
        public float PendingShipment { get; set; }

        [Display(Name = "Weight Receiving")]
        public float WeightReceiving { get; set; }

        [Display(Name = "Qty SO")]
        public float QtySO { get; set; }
        [Display(Name = "Weight SO")]
        public float WeightSO { get; set; }

        [Display(Name = "Qty Shipment")]
        public float QtyShipment { get; set; }
        [Display(Name = "Weight Shipment")]
        public float WeightShipment { get; set; }


        //[Display(Name = "Qty Transfer In")]
        //public float QtyTransferIn { get; set; }
        //[Display(Name = "Qty Transfer Out")]
        //public float QtyTransferOut { get; set; }


        [Display(Name = "Qty On Hand")]
        public float QtyOnhand { get; set; }
        [Display(Name = "Weight On Hand")]
        public float WeightOnhand { get; set; }

        [Display(Name = "Amount Received")]
        public decimal AmountReceived { get; set; }

        [Display(Name = "Amount Shipped")]
        public decimal AmountShipped { get; set; }


        [Display(Name = "Amount On Hand")]
        public decimal AmountOnHand { get; set; }


        [Display(Name = "Avg Amount")]
        public decimal AvgAmount { get; set; }

        [Display(Name = "Stock For Sale")]
        public decimal StockForSale { get; set; }

        [Display(Name = "Product Category")]
        public ProductCategory productCategory { get; set; }




        //2nd Stock Screen
        [Display(Name = "Vendor Name")]
        public string VendorName { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "sales Order Status")]
        public string SalesOrderStatus { get; set; }

        [Display(Name = "Purchase Order Status")]
        public string PurchaseOrderStatus { get; set; }


        [Display(Name = "No. of POs")]
        public float NoOfPO { get; set; }

        [Display(Name = "No. of Received")]
        public float NoOfReceived { get; set; }

        [Display(Name = "Purchased Date")]
        public DateTime PurchasedDate { get; set; }


        [Display(Name = "Sale Date")]
        public DateTime SaleDate { get; set; }


        [Display(Name = "Received Date")]
        public DateTime ReceivedDate { get; set; }

        [Display(Name = "Total Price")]
        public decimal TotalPrice { get; set; }

        [Display(Name = "Total Price")]
        public decimal TotalSalePrice { get; set; }


        [Display(Name = "Total Price")]
        public decimal? PriceInUSD { get; set; }

        [Display(Name = "Total Qty")]
        public float TotalQty { get; set; }

        [Display(Name = "PO No.")]
        public string PONo { get; set; }

        [Display(Name = "SO No.")]
        public string SONo { get; set; }
    }
}
