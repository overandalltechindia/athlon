﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum BusinessSize
    {
        Commercial = 1, 
        Society = 2,
        Dealer = 3, 
        HomeUse = 4, 
    }
}
