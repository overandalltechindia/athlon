﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ReceivingLine : INetcoreBasic
    {
        public ReceivingLine()
        {
            this.createdAt = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Receiving Line Id")]
        public string receivingLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Receiving Id")]
        public string receivingId { get; set; }

        [Display(Name = "Receiving")]
        public Receiving receiving { get; set; }

        [StringLength(38)]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Display(Name = "Warehouse Id")]
        public string warehouseId { get; set; }

        [Display(Name = "Warehouse")]
        public Warehouse warehouse { get; set; }

        [StringLength(38)]
        [Display(Name = "Product Id")]
        public string productId { get; set; }

        [Display(Name = "Product")]
        public Product product { get; set; }

        [Display(Name = "Width")]
        public decimal width { get; set; }

        [Display(Name = "Heigth")]
        public decimal height { get; set; }

        [Display(Name = "Thickness")]
        public decimal thickness { get; set; }

        [Display(Name = "Quantity")]
        public float qty { get; set; }

        [Display(Name = "Total Weight")]
        public float totalWeight { get; set; }

        [Display(Name = "Qty Receive")]
        public float qtyReceive { get; set; }

        [Display(Name = "Weight Receive")]
        public float weightReceive { get; set; }


        [Display(Name = "Price")]
        public decimal price { get; set; }


        [Display(Name = "Total Amount")]
        public decimal totalAmount { get; set; }


        [Display(Name = "Qty Inventory")]
        public float qtyInventory { get; set; }
    }
}
